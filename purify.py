#!/usr/bin/python

""" Define a function called purify that takes in a list of numbers, 
removes all odd numbers in the list, and returns the result.

For example, purify([1,2,3]) should return [2].

Do not directly modify the list you are given as input; 
instead, return a new list with only the even numbers. """

def purify(lst):
	lst = list(lst)
	result = []

	for i in lst:
		if int(i) % 2 == 0:
			result.append(i)
		else:
			pass

	return result

print purify([1,2,3,4,5,7,88])
