#!/usr/bin/python

""" Write a function called median 
that takes a list as an input and returns the median value of the list.

For example: median([1,1,2]) should return 1.

The list can be of any size and the numbers are not guaranteed 
to be in any particular order.
If the list contains an even number of elements, 
your function should return the average of the middle two. """

def median(lst):
	lst = list(lst)
	lst = sorted(lst)

	if len(lst) % 2 == 0:
		first_mid = float(lst[len(lst) / 2 - 1])
		second_mid = float(lst[len(lst) / 2])
		median = float((first_mid + second_mid) / 2)
	else:
		median = lst[len(lst) / 2]

	return median

lst_1 = [5, 2, 3, 1, 4, 6, 90, 1, 9, 7, 8]
lst_2 = [65, 2, 22, 19, 44, 66, 90, 1, 5, 100]

print sorted(lst_1), " - ", len(lst_1), " - ", median(lst_1)
print sorted(lst_2), " - ", len(lst_2), " - ", median(lst_2)