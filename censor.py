#!/usr/bin/python

""" Write a function called censor that takes two strings, text and word, as input. 
It should return the text with the word you chose replaced with asterisks.

For example:

censor("this hack is wack hack", "hack") 

should return

"this **** is wack ****"

Assume your input strings won't contain punctuation or upper case letters.
The number of asterisks you put should correspond to the number of letters in the censored word.
"""

def censor(text, word):
	
	texts = text.split() # list
	
	words = word.split() #list

	asterisk = len(word) * "*" # ****N

	signs = "?!.\'\"`,:;-_/()[]~\n"


	for char in str(text):
		if char not in signs:
			print "No signs in text, continue..."
			continue
		else:
			break 

	for char in str(word):
		if char not in signs:
			continue
		else:
			break

	for item in texts: # every word in text
		if item.split() == words:
			index = texts.index(item)
			texts.insert(index, asterisk)

	return " ".join(texts)

print "this hack is wack hack"
print censor("this hack is wack hack", "hack")