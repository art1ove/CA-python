#!/usr/bin/python

""" Define a function factorial that takes an integer x as input.

Calculate and return the factorial of that number. """

def factorial(x):
	result = 1
	if x == 0:
		return result
	elif x < 0:
		return False
	else:
		while x >= 1:
			result *= x
			x -= 1
	return result

print factorial(4)
