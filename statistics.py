#!/usr/bin/python

""" Creating a program to compute statistics means
	that you won't have to whip out your calculator and manually crunch numbers.
All you'll have to do is supply a new set of numbers and our program
	does all of the hard work.

This mini-project will give you some practice with functions, lists,
	and translating mathematical formulae into programming statements.

In order to use the scores in our program, we'll need them in a container,
	namely a list.

On the right, you'll see the grades listed (see what I did there).
	The data is anonymous to protect the privacy of the students. """

grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def print_grades(grades):
    for grade in grades:
        print grade

def grades_sum(grades):
    total = 0
    for grade in grades:
        total += grade

    return total

def grades_average(grades):
    sum_of_grades = grades_sum(grades)
    average = sum_of_grades / float(len(grades))

    return average

def grades_variance(scores):
    average = grades_average(scores)
    variance = 0

    for score in scores:
        variance += (average - score) ** 2

    variance = variance / len(scores)

    return variance

def grades_std_deviation(variance):

    return variance ** 0.5

variance = grades_variance(grades)

""" We've created quite a few meaningful functions.
Namely, we've created helper functions to print a list of grades,
	compute the sum, average, variance, and standard deviation
	about a set of grades.

Let's wrap up by printing out all of the statistics. """

print print_grades(grades)
print grades_sum(grades)
print grades_average(grades)
print grades_variance(grades)
print grades_std_deviation(variance)
