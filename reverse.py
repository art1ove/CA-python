#!/usr/bin/python

""" Define a function called reverse that takes a string textand returns
	that string in reverse.

For example: reverse("abcd") should return "dcba".

You may not use reversed or [::-1] to help you with this.
You may get a string containing special characters (for example, !, @, or #). 

"""

def reverse(text):
	d = []
	l = []
	text = str(text)

	for char in text:
		d.append(char)

	i = len(d) - 1

	while i >= 0:
		l.append(d[i])
		i -= 1
	return l #"".join(l)

print 'Python!'
print "".join(reverse('Python!'))
